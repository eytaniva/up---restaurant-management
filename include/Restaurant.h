#ifndef RESTAURANT_H_
#define RESTAURANT_H_

#include <vector>
#include <string>
#include "Table.h"
#include "Action.h"


class Restaurant{		
public:
	Restaurant();
    Restaurant(const std::string &configFilePath);
    void start();
	std::size_t getNumOfTables() const;
    Table* getTable(unsigned int ind);
	const std::vector<BaseAction*>& getActionsLog() const;
    std::vector<Dish>& getMenu();
	
	virtual ~Restaurant();
	Restaurant(const Restaurant &other);
	Restaurant& operator=(const Restaurant &other);
	Restaurant(Restaurant &&other);
	Restaurant &operator=(Restaurant &&other);
	std::string inputLine();
	int getLastCustomerIndex()const;
	void setLastCustomerIndex(int id);
	void clearResources();

private:
	int lastCustomerIndex = 0;
    bool open;
    std::vector<Table*> tables;
    std::vector<Dish> menu;
    std::vector<BaseAction*> actionsLog;
	const int MENU_ENRTY_SIZE = 3;


	void skipLines(std::ifstream& stream, std::string& line);
	void copyMenu(std::vector<Dish>& myMenu, const std::vector<Dish>& otherMenu);
	
};

#endif