#ifndef TABLE_H_
#define TABLE_H_

#include <vector>
#include <list>
#include "Customer.h"

typedef std::pair<int, Dish> OrderPair;

class Table{
public:
    Table(int t_capacity);
	size_t getCapacity() const;
    void addCustomer(Customer* customer);
	void addCustomer(Customer* customer, std::vector<OrderPair>* orders);
    void removeCustomer(int id);
    Customer* getCustomer(int id);
    std::vector<Customer*>& getCustomers();
    std::vector<OrderPair>& getOrders();
    void order(const std::vector<Dish> &menu);
    void openTable();
    void closeTable();
    int getBill();
    bool isOpen();
	int getFreeSeats();
	std::vector<OrderPair>* getCustomerOrders(int cId);

	
	virtual ~Table();
	Table(const Table &other);
	Table& operator=(const Table &other);
	Table(Table &&other);
	Table & operator=(Table &&other);
private:
    size_t capacity;
    bool open;
    std::vector<Customer*> customersList;
	std::list<int> removedCustomers;
    std::vector<OrderPair> orderList; 
	void copyOrders(std::vector<OrderPair>& myOrders, const std::vector<OrderPair>& otherOrders);
	void removeIdFromOrderListd(int id);
};


#endif