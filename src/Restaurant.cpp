#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>

#include "../include/Restaurant.h"


Restaurant::Restaurant():lastCustomerIndex(-1),open(false), tables(std::vector < Table*> ()), menu(std::vector<Dish>() ), actionsLog(std::vector<BaseAction*>()) {}
Restaurant::Restaurant(const std::string &configFilePath) : lastCustomerIndex(-1), open(false), tables(std::vector < Table*>()),
															menu(std::vector<Dish>()), actionsLog(std::vector<BaseAction*>()) {
	std::ifstream configFile;
	configFile.open(configFilePath, std::ios::in);

	if (configFile.is_open())
	{
		std::string line;
		std::getline(configFile, line);
		skipLines(configFile, line);

		std::getline(configFile, line);
		skipLines(configFile, line);

		
		std::vector<std::string> tables_vector = ActionFactory::split(line, ',');
		for (size_t i = 0; i < tables_vector.size(); i++)
			 tables.push_back( new Table(std::atoi(tables_vector[i].c_str())));

		skipLines(configFile, line);

		
		int i = 0;
		while (std::getline(configFile, line)){
			skipLines(configFile, line);
			std::vector<std::string> dish_vector = ActionFactory::split(line, ',');
			std::string dishType = dish_vector[1];
			dishType.erase(std::remove_if(dishType.begin(), dishType.end(), isspace), dishType.end());
			menu.push_back(Dish(i, dish_vector[0], std::atoi(dish_vector[2].c_str()), Dish::getDishTypeByName(dishType)));

			i++;
			
		}

		skipLines(configFile, line);
	}


}


void Restaurant::start() {
	std::cout << "Restaurant is now open!" << std::endl;
	open = true;
	std::string line;
	while (line != "closeall" ) {
		line = inputLine();
		BaseAction* action = ActionFactory::createAction(line,*this);
		action->act(*this);
		this->actionsLog.push_back(action);
	}
}

std::size_t Restaurant::getNumOfTables() const {
	return tables.size();
}
Table* Restaurant:: getTable(unsigned int ind) { 
	if (ind >= getNumOfTables())
		return nullptr;
	return tables[ind];
}
const std::vector<BaseAction*>& Restaurant:: getActionsLog() const { 
	return  actionsLog;
}

std::vector<Dish>& Restaurant::getMenu() {
	return menu;
}

 Restaurant::~Restaurant() {
	clearResources();
}

Restaurant::Restaurant(const Restaurant &other) :lastCustomerIndex(other.getLastCustomerIndex()), open(other.open), tables(std::vector < Table*>()), menu(std::vector<Dish>()), actionsLog(std::vector<BaseAction*>()) {
	copyMenu(this->menu, other.menu);

	for  (Table *t : other.tables)
	{
		this->tables.push_back(new Table(*t));
	}
	for  (BaseAction *ba : other.actionsLog)
	{
		this->actionsLog.push_back(ActionFactory::copyAction(ba));
	}

}

 Restaurant& Restaurant::operator=(const Restaurant &other) {
	if (this == &other)
		return *this;

	this->clearResources();
	this->open = other.open;
	copyMenu(this->menu, other.menu);

	for  (Table *t : other.tables)
	{
		this->tables.push_back(new Table(*t));
	}
	for  (BaseAction *ba : other.actionsLog)
	{
		this->actionsLog.push_back(ActionFactory::copyAction(ba));
	}

	return *this;

}

Restaurant::Restaurant(Restaurant &&other) :lastCustomerIndex(other.getLastCustomerIndex()), open(other.open), tables(std::vector < Table*>()), menu(std::vector<Dish>()), actionsLog(std::vector<BaseAction*>()) {

	
	copyMenu(this->menu, other.menu);


	for  (Table *t : other.tables)
	{
		this->tables.push_back(t);
		t = nullptr;
	}
	for  (BaseAction *ba : other.actionsLog)
	{
		this->actionsLog.push_back(ba);
		ba = nullptr;
	}

}

Restaurant &Restaurant::operator=(Restaurant &&other) {
	if (this != &other)
	{
		this->clearResources();
		

		this->open = other.open;
		copyMenu(this->menu, other.menu);

		for  (Table *t : other.tables)
		{
			this->tables.push_back(t);
			t = nullptr;
		}
		for  (BaseAction *ba : other.actionsLog)
		{
			this->actionsLog.push_back(ba);
			ba = nullptr;
		}
	}
	return *this;
}

std::string Restaurant::inputLine()
{
	std::string line;
	std::getline(std::cin, line);
	return line;
}

int Restaurant::getLastCustomerIndex() const
{
	return this->lastCustomerIndex;
}

void Restaurant::setLastCustomerIndex(int id) 
{
	this->lastCustomerIndex = id;
}

void Restaurant::skipLines(std::ifstream& stream, std::string& line) {
	while ( line == "\n" || line == "\r\n" || line[0] == '#' )
		std::getline(stream, line);
}


void Restaurant::copyMenu(std::vector<Dish>& myMenu,const std::vector<Dish>& otherMenu) {
	myMenu.clear();
	for (Dish d : otherMenu)
		myMenu.push_back(Dish(d.getId(), d.getName(), d.getPrice(), d.getType()));
}

void Restaurant::clearResources()
{
	
	for (Table *t : this->tables)
	{
		if(t != nullptr)
			delete t;
	}
	for (BaseAction *ba : this->actionsLog)
	{
		if (ba != nullptr)
			delete ba;
	}
	this->tables.clear();
	this->actionsLog.clear();
}

