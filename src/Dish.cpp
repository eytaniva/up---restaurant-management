#include "../include/Dish.h"
#include <fstream>
#include <iostream>
#include <string>

#include "../include/Restaurant.h"


Dish::Dish(int d_id, std::string d_name, int d_price, DishType d_type) :id(d_id), name(d_name), price(d_price), type(d_type) {}
int Dish::getId() const { return this->id; }
std::string Dish::getName() const { return this->name; }
int Dish::getPrice() const { return this->price; }
DishType Dish::getType() const { return this->type; }
DishType Dish::getDishTypeByName(std::string name) {
	if (name.compare("VEG") == 0 || name.compare("veg") == 0)
		return VEG;
	else if (name.compare("ALC") == 0 || name.compare("alc") == 0)
		return ALC;
	else if (name.compare("SPC") == 0 || name.compare("spc") == 0)
		return SPC;
	else  
		return BVG;
}
std::string Dish::getDishNameByType(DishType type)
{
	if (type == VEG)
		return "VEG";
	else if (type == ALC)
		return "ALC";
	else if (type == SPC)
		return "SPC";
	else  
		return "BVG";
}
bool Dish::operator==(const Dish& dish) const {
	return this->id == dish.id && this->name == dish.name && this->price == dish.price && this->type == dish.type;
}

