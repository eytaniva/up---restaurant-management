#include "../include/Table.h"
#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>



Table::Table(int t_capacity) : capacity(t_capacity),open(false),customersList(std::vector<Customer*>()),removedCustomers(std::list<int>()),orderList(std::vector<OrderPair>()) {}
size_t Table::getCapacity() const { return this->capacity; }
void Table::addCustomer(Customer* customer) {
	if (this->capacity > this->customersList.size())
		this->customersList.push_back(customer);
}
void Table::addCustomer(Customer* customer, std::vector<OrderPair>* ol) {
	if (this->capacity != this->customersList.size()){
		this->customersList.push_back(customer);
		for (OrderPair op : *ol) {
			this->orderList.push_back(OrderPair(op.first, op.second));
		}
		delete ol;
	}
}
void Table::removeCustomer(int id) {
	if (this->customersList.size() != 0) {

		for (auto it = this->customersList.begin(); it != this->customersList.end(); ) {
			Customer * current = (*it);
			if (current->getId()== id) {
				it = this->customersList.erase(it);
			}
			else {
				++it;
			}
		}
	}
	removeIdFromOrderListd(id);		
}


Customer* Table::getCustomer(int id) {
	Customer *ans = nullptr;
		for  (Customer *c : this->customersList)
		{
			if (c->getId() == id)
			{
				ans = c;
			}
		}
	return ans;
}
std::vector<Customer*>& Table::getCustomers() {
	return this->customersList;
}
std::vector<OrderPair>& Table::getOrders() {
	return this->orderList;
}
void Table::order(const std::vector<Dish> &menu) {
	std::vector<int> cOrder;
	std::vector<Dish> cOrderDishes;
	for  (Customer *c : this->customersList)
	{
		cOrder = c->order(menu);
		for (size_t i = 0; i < cOrder.size() ; i++)
		{
			for  (Dish d : menu)
			{
				if (d.getId() == cOrder[i])
				{
					this->orderList.push_back(OrderPair(c->getId(), d));
					std::cout << c->getName() + " ordered " + d.getName() << std::endl;
					break;
				}
			}
		}
	}
}
void Table::openTable() {
	this->open = true;
}
void Table::closeTable() {
	this->open = false;
	for  (Customer *c : this->customersList)
	{
		if(c != nullptr)
		delete c;
	}
	this->customersList.clear();
}
int Table::getBill() {
	int sum = 0;
	for  (OrderPair op : this->orderList)
	{
		sum = sum + op.second.getPrice();
	}
	return sum;
}
bool Table::isOpen() { return this->open; }

int Table::getFreeSeats() { return this->capacity - this->customersList.size(); }

std::vector<OrderPair>* Table::getCustomerOrders(int cId) {
	std::vector<OrderPair>* ans = new std::vector<OrderPair>();
	for  (OrderPair op : this->orderList)
	{
		if (op.first == cId)
		{
			ans->push_back(op);
		}
	}
	return ans;
}


 Table::~Table() {
	for  (Customer *c : this->customersList)
	{
		delete c;

	}
}

 Table::Table(const Table &other):capacity(other.getCapacity()),open(other.open), customersList(std::vector<Customer*>()),removedCustomers(std::list<int>()),orderList(std::vector<OrderPair>()) {
	 copyOrders(this->orderList, other.orderList);
	 for  (Customer *c : other.customersList){
		Customer * cus = CustomerFactory::createCustomer(c->getType(), c->getName(), c->getId());
		this->customersList.push_back(cus);
	 }
}

Table& Table::operator=(const Table &other) {
	if (this == &other)
		return *this;


	for (Customer *c : this->customersList)
	{
		delete c;
	}
	this->capacity = other.getCapacity();
	this->open = other.open;
	this->removedCustomers = other.removedCustomers;
	copyOrders(this->orderList, other.orderList);

	this->customersList.clear();
	for  (Customer *c : other.customersList)
	{
		Customer * cus = CustomerFactory::createCustomer(c->getType(), c->getName(), c->getId());
		this->customersList.push_back(cus);
	}


	return *this;

};

Table::Table(Table &&other) :capacity(other.getCapacity()), open(other.open), customersList(std::vector<Customer*>()), removedCustomers(std::list<int>()), orderList(std::vector<OrderPair>()) {
	copyOrders(this->orderList, other.orderList);
	for  (Customer *c : other.customersList)
	{
		this->customersList.push_back(c);
		c = nullptr;
	}
}

Table & Table::operator=(Table &&other) {
	if (this != &other) {
		for (Customer *c : this->customersList)
		{
			delete c;
		}

		this->orderList.clear();
		this->customersList.clear();

		this->capacity = other.getCapacity();
		this->open = other.open;
		this->removedCustomers = other.removedCustomers;
		copyOrders(this->orderList, other.orderList);
		for  (Customer *c : other.customersList)
		{
			this->customersList.push_back(c);
			c = nullptr;
		}

		
	}
	return *this;
}

void Table::copyOrders(std::vector<OrderPair>& myOrders, const std::vector<OrderPair>& otherOrders) {
	myOrders.clear();
	for (OrderPair d : otherOrders)
		 myOrders.push_back(OrderPair(d.first, d.second));
}

void Table::removeIdFromOrderListd(int id) {
	std::vector<OrderPair> newOrderList;
	for (OrderPair op : this->orderList) {
		if (op.first != id) {
			newOrderList.push_back(op);
		}
	}
	copyOrders( this->orderList , newOrderList);
}