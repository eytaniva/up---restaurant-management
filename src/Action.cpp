#include <string>
#include <iostream>
#include "../include/Restaurant.h"




extern Restaurant* backup; 
BaseAction::BaseAction():errorMsg(""),status(PENDING) { }
ActionStatus BaseAction::getStatus() const{ return status; } 
void BaseAction::complete(){
	this->status = COMPLETED;
}
void BaseAction::error(std::string errorMsg) {
	this->errorMsg = errorMsg;
	this->status = ERROR;
}
std::string BaseAction::getErrorMsg() const {
	std::stringstream ss;
	ss << "Error: " << this->errorMsg;
	return ss.str();
}
std::string BaseAction::getEndMessage()const {
	if (this->getStatus() == COMPLETED)
		return "Completed";
	else
		return getErrorMsg();
}
BaseAction::~BaseAction()
{
}




OpenTable::OpenTable(int id, std::vector<Customer *> &customersList): tableId(id),customers(customersList){}
void OpenTable::act(Restaurant &restaurant){
	if(this->getStatus() == PENDING){
		Table* table = restaurant.getTable(this->tableId);
		if (table == nullptr || table->isOpen()) {
			this->error("Table does not exist or is already open");
			std::cout<< this->getErrorMsg() << std::endl;
			for(Customer* cus : customers)
				delete cus;
			customers.clear();
		}
		else if (table->getCapacity() < customers.size()) {
			this->error("requested table doesn't have enough seats.");
			std::cout << this->getErrorMsg() << std::endl;
			for(Customer* cus : customers)
				delete cus;
			customers.clear();
		}
		else {
			
			for (Customer* cus : customers)
				table->addCustomer(cus);
			table->openTable();
			this->complete();
		}
	}
}
std::string OpenTable::toString() const {

		std::stringstream ss;
		ss << "Open " << tableId <<" " ;
		for (Customer* cus : customers)
			ss << cus->getName() << "," << cus->getType()<<" " ;
		ss << getEndMessage();
		
		return   ss.str() ;
	}
BaseAction* OpenTable::deepCopy() {
	std::vector<Customer *> customersCopy;
	OpenTable * copy = new OpenTable(tableId, customersCopy);
	if (this->getStatus() == ERROR)
		copy->error(this->getErrorMsg().substr(6));
	else if (this->getStatus() == COMPLETED)
		copy->complete();
	return copy;
}





Order::Order(int id):tableId(id){}
void Order::act(Restaurant &restaurant) {
	if(this->getStatus() == PENDING){
		Table* table = restaurant.getTable(this->tableId);
		if (table == nullptr || !table->isOpen()) {
			this->error("Table does not exist or is not open");
			std::cout <<  this->getErrorMsg() << std::endl;
		}
		else {
			table->order(restaurant.getMenu());
			this->complete();
		}
	}
}
std::string Order::toString() const {
		std::stringstream ss;
		ss << tableId;
		return "Order " + ss.str() +" "+getEndMessage();
	};
BaseAction* Order::deepCopy() {
		Order * copy = new Order(tableId);
		if (this->getStatus() == ERROR)
			copy->error(this->getErrorMsg().substr(6));
		else if (this->getStatus() == COMPLETED)
			copy->complete();
		return copy;
	}




MoveCustomer::MoveCustomer(int src, int dst, int customerId):srcTable(src),dstTable(dst),id(customerId){}
void MoveCustomer::act(Restaurant &restaurant) {
	if(this->getStatus() == PENDING){
		Table* srcTableInst = restaurant.getTable(this->srcTable);
		Table* dstTableInst = restaurant.getTable(this->dstTable);

		if (srcTableInst == nullptr || dstTableInst == nullptr || !dstTableInst->isOpen() ||
			!srcTableInst->isOpen() || srcTableInst->getCustomer(id) == nullptr || dstTableInst->getFreeSeats() < 1) {
			this->error("Cannot move customer ");
			std::cout <<  this->getErrorMsg() << std::endl;
		}
		else{
			Customer* customer = srcTableInst->getCustomer(id);
			
			std::vector<OrderPair>* orders = srcTableInst->getCustomerOrders(id);
			
			srcTableInst->removeCustomer(id);		
			if ((srcTableInst->getCustomers()).empty())
				srcTableInst->closeTable();
			
			dstTableInst->addCustomer(customer,orders);
			this->complete();
		}

	}
}
std::string MoveCustomer::toString() const {
		std::stringstream ss;
		ss << "move " << srcTable << " " << dstTable << " " << id ;
		return  ss.str()+" "+getEndMessage();
	};
BaseAction* MoveCustomer::deepCopy() {
		MoveCustomer * copy = new MoveCustomer(srcTable, dstTable,id);
		if (this->getStatus() == ERROR)
			copy->error(this->getErrorMsg().substr(6));
		else if (this->getStatus() == COMPLETED)
			copy->complete();
		return copy;
	}




Close::Close(int id):tableId(id){}
void Close::act(Restaurant &restaurant) {
	if(this->getStatus() == PENDING){
		Table* table = restaurant.getTable(this->tableId);
		if (table == nullptr  ||	!table->isOpen() ){
			this->error("Table does not exist or is not open");
			std::cout <<  this->getErrorMsg();
		}
		else {
			std::cout<< "Table "<<tableId<< " was closed. Bill " <<table->getBill()<<"NIS" << std::endl;
			table->closeTable();
			this->complete();
		}
	}
}
std::string Close::toString() const {
		std::stringstream ss;
		ss << "close " << tableId;
		return  ss.str() + " " + getEndMessage();
	}
BaseAction* Close::deepCopy() {
		Close * copy = new Close(tableId);
		if (this->getStatus() == ERROR)
			copy->error(this->getErrorMsg().substr(6));
		else if (this->getStatus() == COMPLETED)
			copy->complete();
		return copy;
	}




CloseAll::CloseAll(){}
void CloseAll::act(Restaurant &restaurant) {
	if(this->getStatus() == PENDING){
		size_t num = restaurant.getNumOfTables();
		for (size_t i = 0; i < num; i++) {
			Table * table = restaurant.getTable(i);
			if (table != nullptr && table->isOpen()) {
				std::cout << "Table " << i << " was closed. Bill " << table->getBill() << "NIS" << std::endl;
				table->closeTable();
			}
		}
		this->complete();
	}
}
std::string CloseAll::toString() const {
		std::stringstream ss;
		ss << "closeall";
		return  ss.str() + " " + getEndMessage();
	}
BaseAction* CloseAll::deepCopy() {
	CloseAll * copy = new CloseAll();
	if (this->getStatus() == ERROR)
		copy->error(this->getErrorMsg().substr(6));
	else if (this->getStatus() == COMPLETED)
		copy->complete();
	return copy;
}




PrintMenu::PrintMenu(){}
void PrintMenu::act(Restaurant &restaurant) {
		std::vector<Dish>& menu = restaurant.getMenu();
		for (Dish dish : menu) {
			std::cout << dish.getName() << " " << Dish::getDishNameByType(dish.getType()) << " " << dish.getPrice() << "NIS"<< std::endl;
		}
		this->complete();
	}
std::string PrintMenu::toString() const {
		std::stringstream ss;
		ss << "menu";
		return  ss.str() + " " + getEndMessage();
	}
BaseAction* PrintMenu::deepCopy() {
	PrintMenu * copy = new PrintMenu();
	if (this->getStatus() == ERROR)
		copy->error(this->getErrorMsg().substr(6));
	else if (this->getStatus() == COMPLETED)
		copy->complete();
	return copy;
}



PrintTableStatus::PrintTableStatus(int id):tableId(id){}
void PrintTableStatus::act(Restaurant &restaurant) {
	if(this->getStatus() == PENDING){
		Table* table = restaurant.getTable(this->tableId);
		if (table != nullptr ) {
			if( !table->isOpen())
				std::cout << "Table "<<tableId << " status: closed"<<std::endl;
			else {
				std::cout << "Table " << tableId << " status: open" << std::endl;
				std::vector<Customer*> customers = table->getCustomers();
				std::cout << "Customers:"  << std::endl;
				for (Customer* customer : customers) {
					std::cout << customer->getId() << " " << customer->getName() << std::endl;
				}
				std::vector<OrderPair>& orders = table->getOrders();
				std::cout << "Orders:" << std::endl;
				for (OrderPair order : orders) {
					std::cout << order.second.getName() << " " << order.second.getPrice()<<"NIS "<<order.first << std::endl;
				}
				std::cout << "Current Bill: " << table->getBill() <<"NIS"<< std::endl;
			}

		}
		this->complete();
	}
}
std::string PrintTableStatus::toString() const {
		std::stringstream ss;
		ss << "status "  << tableId;
		return  ss.str() + " " + getEndMessage();
	}
BaseAction* PrintTableStatus::deepCopy() {
	PrintTableStatus * copy = new PrintTableStatus(tableId);
	if (this->getStatus() == ERROR)
		copy->error(this->getErrorMsg().substr(6));
	else if (this->getStatus() == COMPLETED)
		copy->complete();
	return copy;
}




PrintActionsLog::PrintActionsLog(){}
void PrintActionsLog::act(Restaurant &restaurant) {
	if(this->getStatus() == PENDING){
		std::stringstream ss;
		const std::vector<BaseAction*> actions =  restaurant.getActionsLog();
		for (BaseAction* action : actions) {
			ss << action->toString()<<std::endl;
		}
		this->complete();
		this->toString();
		std::cout << ss.str();
	}
}
std::string PrintActionsLog::toString() const {
		return "log " + getEndMessage();
	}
BaseAction* PrintActionsLog::deepCopy() {
	PrintActionsLog * copy = new PrintActionsLog();
	if (this->getStatus() == ERROR)
		copy->error(this->getErrorMsg().substr(6));
	else if (this->getStatus() == COMPLETED)
		copy->complete();
	return copy;
}




BackupRestaurant::BackupRestaurant() {}
void BackupRestaurant::act(Restaurant &restaurant) {
	if(this->getStatus() == PENDING){
		if(backup != nullptr)
			delete backup;
		backup = new Restaurant(restaurant);
		complete();
	}
}
std::string BackupRestaurant::toString() const {
		return "backup " + getEndMessage();
	}
BaseAction* BackupRestaurant::deepCopy() {
	BackupRestaurant * copy = new BackupRestaurant();
	if (this->getStatus() == ERROR)
		copy->error(this->getErrorMsg().substr(6));
	else if (this->getStatus() == COMPLETED)
		copy->complete();
	return copy;
}





RestoreResturant::RestoreResturant(){}
void RestoreResturant::act(Restaurant &restaurant) {
	if(this->getStatus() == PENDING){
		if (backup == nullptr) {
			error("No backup Available");
			std::cout << this->getErrorMsg() << std::endl;
		}
		else {
			
			restaurant = (*backup);
			complete();
		}
	}
}
std::string RestoreResturant::toString() const {
		return "restore " + getEndMessage();
	}
BaseAction* RestoreResturant::deepCopy() {
	RestoreResturant * copy = new RestoreResturant();
	if (this->getStatus() == ERROR)
		copy->error(this->getErrorMsg().substr(6));
	else if (this->getStatus() == COMPLETED)
		copy->complete();
	return copy;
}


BaseAction* ActionFactory::createAction(std::string message, Restaurant & rest) {
		std::string keyword = message.substr(0, message.find_first_of(' '));
		BaseAction* action = nullptr;
		if (keyword =="open")
			action = createOpenTableByStr(message,rest);
		else if (keyword == "order")
			action = createOrder(message);
		else if (keyword == "move")
			action = createMoveCustomer(message);
		else if (keyword == "close")
			action = createClose(message);
		else if (keyword == "closeall")
			action = new CloseAll();
		else if (keyword == "status")
			action = createPrintTableStatus(message);
		else if (keyword == "log")
			action = new PrintActionsLog();
		else if (keyword == "menu")
			action = new PrintMenu();
		else if (keyword == "backup")
			action = new BackupRestaurant();
		else if (keyword == "restore")
			action = new RestoreResturant();
		else
			std::cout<< "SUPERERROR !!! with input \""<< keyword <<"\" for action creation"<<std::endl;

		return action;
	}
BaseAction* ActionFactory::copyAction(BaseAction* action) {
	return action->deepCopy();
}
OpenTable* ActionFactory::createOpenTableByStr(std::string message, Restaurant & rest) {
		std::vector<std::string> splittedMessage = split(message, ' ');
		int tableId = atoi(splittedMessage.at(1).c_str());
		std::vector<Customer*> customers;
		int i = 0;
		int lastCustomerIndex = rest.getLastCustomerIndex();
		for (std::string var : splittedMessage)
		{
			
			if (i >= 2) {
				std::string name = split(var, ',').at(0);
				std::string type = split(var, ',').at(1);
				Customer* customer = CustomerFactory::createCustomer(type, name, lastCustomerIndex + 1);
				lastCustomerIndex++;
				if (customer != nullptr)
					customers.push_back(customer);
				
			}
			i++;
		}
		rest.setLastCustomerIndex(lastCustomerIndex);
		return new OpenTable(tableId, customers);
	}
Order* ActionFactory::createOrder(std::string message) {
		std::vector<std::string> splittedMessage = split(message, ' ');
		int tableId = atoi(splittedMessage.at(1).c_str());
		return new Order(tableId);
	}
MoveCustomer* ActionFactory::createMoveCustomer(std::string message) {
		std::vector<std::string> splittedMessage = split(message, ' ');
		int src = atoi(splittedMessage.at(1).c_str());
		int dst = atoi(splittedMessage.at(2).c_str());
		int customer = atoi(splittedMessage.at(3).c_str());
		return new MoveCustomer(src,dst,customer);
	}
Close* ActionFactory::createClose(std::string message) {
		std::vector<std::string> splittedMessage = split(message, ' ');
		int tableId = atoi(splittedMessage.at(1).c_str());
		return new Close(tableId);
	}
PrintTableStatus* ActionFactory::createPrintTableStatus(std::string message) {
		std::vector<std::string> splittedMessage = split(message, ' ');
		int tableId = atoi(splittedMessage.at(1).c_str());
		return new PrintTableStatus(tableId);
	}
void ActionFactory::split(const std::string &s, char delim, std::back_insert_iterator<std::vector<std::string>> result) {
		std::stringstream ss(s);
		std::string item;
		while (std::getline(ss, item, delim)) {
			*(result++) = item;
		}
	}
std::vector<std::string> ActionFactory::split(const std::string &s, char delim) {
		std::vector<std::string> elems;
		split(s, delim, std::back_inserter(elems));
		return elems;
	}

