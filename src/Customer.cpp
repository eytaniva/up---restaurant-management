#include "../include/Customer.h"
#include <fstream>
#include <iostream>
#include <string>



Customer::Customer(std::string c_name, int c_id) : customerIndex(0), name(c_name), id(c_id){};
std::string Customer::getName() const { return this->name; }
int Customer::getId() const { return this->id; }
Customer::~Customer()
{
}

void Customer::changeIndex() { customerIndex = customerIndex + 1; }


VegetarianCustomer::VegetarianCustomer(std::string name, int id) : Customer(name, id) {};
std::vector<int> VegetarianCustomer::order(const std::vector<Dish> &menu) {
	std::vector<int> ans;
	bool orderedAVEG = false;
	int expBVGID =-1;
	int expBVGPrice = -1;
	
	for (Dish d : menu)
	{
		if (d.getType() == VEG && !(orderedAVEG)) {
			ans.push_back(d.getId());
			orderedAVEG = true;
		}

		if (expBVGID == -1 && d.getType() == BVG) {
			expBVGID = d.getId();
			expBVGPrice = d.getPrice();
		}
		
		if (expBVGID != -1 && d.getType() == BVG)
			if (d.getPrice() > expBVGPrice) {
				expBVGID = d.getId();
				expBVGPrice = d.getPrice();
			}
			
	}
	ans.push_back(expBVGID);
	return ans;
}
std::string VegetarianCustomer::toString() const {
	return this->getName() + "," + std::to_string(this->getId()) + " is veg";
}
std::string VegetarianCustomer::getType() const { return "veg"; }

VegetarianCustomer::~VegetarianCustomer()
{
}



CheapCustomer::CheapCustomer(std::string name, int id) : Customer(name, id), isOrdered(false) {}
std::vector<int> CheapCustomer::order(const std::vector<Dish> &menu) {
	std::vector<int> ans;
	int cheapestDish;
	int cheapestPrice;

	if (!(this->isOrdered)) {
		
		for  (Dish d : menu)
		{
			if (!isOrdered) {
				cheapestDish = d.getId();
				cheapestPrice = d.getPrice();
				isOrdered = true;
			}

			if ( d.getPrice() < cheapestPrice) {
				cheapestDish = d.getId();
			cheapestPrice = d.getPrice();
			}
		}
		ans.push_back(cheapestDish);
		
	}
	return ans;
	
}
std::string CheapCustomer::toString() const
{
	return this->getName() + "," + std::to_string(this->getId()) + " is cheap";
}

std::string CheapCustomer::getType()const { return "chp"; }

CheapCustomer::~CheapCustomer()
{
}



SpicyCustomer::SpicyCustomer(std::string name, int id) : Customer(name, id),isFirst(true) {};
std::vector<int> SpicyCustomer::order(const std::vector<Dish> &menu) {
	std::vector<int> ans;
	int expSPCID = -1;
	int expSPCPrice = -1;

	int cheapBVGID = -1;
	int cheapBVGPrice = -1;

	if (isFirst) {
		for (Dish d : menu)
		{
			if (d.getType() == SPC) {
				if (expSPCID == -1 ) {
					expSPCID = d.getId();
					expSPCPrice = d.getPrice();
				}
				else if (d.getPrice() > expSPCPrice) {
						expSPCID = d.getId();
						expSPCPrice = d.getPrice();
				}
			}

		}
		this->isFirst = false;
		ans.push_back(expSPCID);
	}
	else {
		for (Dish d : menu)
		{
			if (d.getType() == BVG) {
				if (cheapBVGID == -1) {
					cheapBVGID = d.getId();
					cheapBVGPrice = d.getPrice();
				}
				else if (d.getPrice() < cheapBVGPrice) {
					cheapBVGID = d.getId();
					cheapBVGPrice = d.getPrice();
				}
			}
		}
		ans.push_back(cheapBVGID);
	}
	return ans;
}
std::string SpicyCustomer::toString() const {
	return this->getName() + "," + std::to_string(this->getId()) + " is spicy";
}

std::string SpicyCustomer::getType()const { return "spc"; }

SpicyCustomer::~SpicyCustomer()
{
}




AlchoholicCustomer::AlchoholicCustomer(std::string name, int id) : Customer(name, id),lastPrice(-1),lastId(-1) {};
std::vector<int> AlchoholicCustomer::order(const std::vector<Dish> &menu) {
	std::vector<int> ans;
	bool firstrun = true;
	int cheapALCID = -1;
	int cheapALCPrice;

	for (Dish d : menu)
	{
		if (d.getType() == ALC && d.getPrice() >= this->lastPrice
			&& d.getId() != this->lastId) {
			if (firstrun ) {
				cheapALCID = d.getId();
				cheapALCPrice = d.getPrice();
				firstrun = false;
			}
			else if (d.getPrice() < cheapALCPrice) {
				cheapALCID = d.getId();
				cheapALCPrice = d.getPrice();
			}
		}
	}

	if (cheapALCID != -1){
		ans.push_back(cheapALCID);
		this->lastPrice = cheapALCPrice;
		this->lastId = cheapALCID;
		}
	return ans;
}
std::string AlchoholicCustomer::toString() const {
	return this->getName() + "," + std::to_string(this->getId()) + " is alcoholic";
}
std::string AlchoholicCustomer::getType() const{ return "alc"; }

AlchoholicCustomer::~AlchoholicCustomer(){}



 Customer*  CustomerFactory::createCustomer(std::string type,std::string name,int id) {
	Customer* customer = nullptr;
	if (type == "veg" || type == "VEG")
		customer = new VegetarianCustomer(name, id);
	else if (type == "chp" || type == "CHP")
		customer = new CheapCustomer(name, id);
	else if (type == "spc" || type == "SPC")
		customer = new SpicyCustomer(name, id);
	else if (type == "alc" || type == "ALC")
		customer = new AlchoholicCustomer(name, id);
	return customer;
}
